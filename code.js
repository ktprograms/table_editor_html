window.onload = () => {
	/** @type {HTMLTextAreaElement} */
	const text = document.getElementById('text');

	/** @type {string[][]} */
	let table = [[]];
	/** @type {number[]} */
	let maxWidths = [];

	text.addEventListener('keydown', e => {
		if (e.keyCode === 13 && e.shiftKey) { // Shift+Enter
			tableFirstRowDone();
			e.preventDefault();
		} else if (e.keyCode === 9 && e.shiftKey) { // Shift+Tab
			moveNextCell();
			e.preventDefault();
		}
	});

	////////////////////
	// Event handlers //
	////////////////////

	const tableFirstRowDone = () => {
		const rawHeaderText = text.value.split('\n')[0];
		const headerTextWhitespaceRemoved = rawHeaderText.replace(/ *\| */g, '|');
		// Because '|' is the first and last char in the string,
		// remove the resulting empty values after the split
		table[0] = headerTextWhitespaceRemoved.split('|').slice(1, -1);

		// Add an empty table row
		table.push(new Array(table[0].length).fill(''));

		// Generate the table
		reformatTable();

		/* Move the cursor to be ready to insert into the empty cell */
		// No easy to get/set the cursor to a line, so manually calculate it
		// by multiplying the width of one row by the number of rows
		// (excluding the new empty row since that's the one the cursor will
		// end up on)
		const tableWidth = maxWidths.reduce((acc, e) => (
			(e + 3) + acc // 3 is the '|' + one space on the left and right
		), 2); // 2 is:
		// - The other '|' on the outer edges of the table
		// - The newline after each row
		// - To get past the '|' and space on the destination line
		//
		// Calculate the number of characters from the start of the tableText
		// until the new insertion point.
		// The length of the table is used even though the last line shouldn't
		// be counted because the separator line isn't part of the table,
		// so the plus one evens out with the minus one.
		// The adding two is to get past the '|' and space on the destination
		// line.
		const newCursorPosition = (tableWidth * table.length) + 2;
		// Set the cursor position
		text.selectionStart = text.selectionEnd = newCursorPosition;
	};

	const moveNextCell = () => {
		/* Find the cell the cursor is currently in */
		// Get the current cursor position
		const cursorIndex = text.selectionStart;
		// Following line of code modified from https://stackoverflow.com/a/9185820
		const textBeforeCursorLines = text.value.substr(0, cursorIndex).split('\n')
		// The minus one is ignoring the line the cursor
		const cursorRow = textBeforeCursorLines.length - 1;
		const cursorCol = textBeforeCursorLines[cursorRow].length;

		// Get the text before the cursor on the current line
		const strRow = text.value.split('\n')[cursorRow];
		const strBeforeCursor = strRow.slice(0, cursorCol);

		// Use that text to calculate how many '|'s come before the cursor,
		// and therefore which cell the cursor is in
		let numPipes = 0;
		for (const c of strBeforeCursor) {
			if (c === '|') {
				numPipes++;
			}
		}
		// The 1 is because after the first '|',
		// you're still only in the first cell (index 0)
		const curTableCol = numPipes - 1;
		// Subtract counting the separator row ONLY IF
		// the cursor isn't on row 0 (the header row)
		const curTableRow = (cursorRow === 0) ? cursorRow : (cursorRow - 1);

		// Get the text of the current cell
		const strRowCondensed = strRow.replace(/ *\| */g, '|');
		const rowCells = strRowCondensed.split('|').slice(1, -1);
		const cellText = rowCells[curTableCol];

		table[curTableRow][curTableCol] = cellText;

		reformatTable();

		// Figure out the new cursor position
		const usedWidths = maxWidths.slice(0, (curTableCol + 1));
		let newCursorCol = 0;
		for (const width of usedWidths) {
			// 3 is the '|' + one space on the left and right of the content
			newCursorCol += width + 3;
		}
		// 2 is the '|' and one space in the next empty cell
		newCursorCol += 2;

		// Put the cursor at the calculated position
		const newTableWidth = maxWidths.reduce((acc, e) => (
			(e + 3) + acc
		), 2);
		const newCursorPosition = (newTableWidth * table.length) + newCursorCol;
		text.selectionStart = text.selectionEnd = newCursorPosition;
	};

	//////////////////////
	// Helper functions //
	//////////////////////

	const reformatTable = () => {
		// Calculate the widest each column is
		maxWidths = [];
		for (let c = 0; c < table[0].length; c++) {
			let maxWidth = 0;
			for (const row of table) {
				const width = row[c].length;
				if (width > maxWidth) {
					maxWidth = width;
				}
			}
			maxWidths.push(maxWidth);
		}

		let tableText = '';

		// Header row
		for (let c = 0; c < table[0].length; c++) {
			const cell = table[0][c];
			const extraWhitespaceCount = maxWidths[c] - cell.length;
			const extraWhitespace = ' '.repeat(extraWhitespaceCount);
			tableText += `| ${cell} ${extraWhitespace}`;
		}
		tableText += '|\n';

		// Separator row
		for (let c = 0; c < table[0].length; c++) {
			// 2 is the one space on the left and right of the content
			const dashesCount = maxWidths[c] + 2;
			const dashes = '-'.repeat(dashesCount);
			tableText += `|${dashes}`;
		}
		tableText += '|\n';

		// Content rows
		const tableNoHead = table.slice(1);
		for (const row of tableNoHead) {
			for (let c = 0; c < tableNoHead[0].length; c++) {
				const cell = row[c];
				const extraWhitespaceCount = maxWidths[c] - cell.length;
				const extraWhitespace = ' '.repeat(extraWhitespaceCount);
				tableText += `| ${cell} ${extraWhitespace}`;
			}
			tableText += '|\n';
		}

		// Output the formatted table
		text.value = tableText;
	};
};
