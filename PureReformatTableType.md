# The Type Definition for the reformatTable function (using Typescript)

(I haven't set up a typescript compiler so I don't know if this is valid syntax)

```ts
reformatTable: (table: string[][], cursorPos: number) => { tableText: string, newTable: string[][], newCursorPos: number }
```
